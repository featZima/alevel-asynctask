package demo.application.lection27;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class PromptDialogFragment extends DialogFragment {


    protected PromptDialogCallback callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (PromptDialogCallback) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callback = (PromptDialogCallback) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public PromptDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_prompt_dialog, container, false);
        fragmentView
                .findViewById(R.id.cancelButton)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (callback != null) {
                            callback.onCancel();
                            getDialog().dismiss();
                        }
                    }
                });
        fragmentView
                .findViewById(R.id.deleteButton)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (callback != null) {
                            callback.onDelete();
                            getDialog().dismiss();
                        }
                    }
                });
        return fragmentView;
    }

    public static PromptDialogFragment newInstance() {
        return new PromptDialogFragment();
    }
}
