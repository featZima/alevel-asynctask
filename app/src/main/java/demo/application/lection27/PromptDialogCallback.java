package demo.application.lection27;

public interface PromptDialogCallback {
    void onCancel();
    void onDelete();
}
