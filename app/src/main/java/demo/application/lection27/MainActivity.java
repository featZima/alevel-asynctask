package demo.application.lection27;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.StringWriter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements PromptDialogCallback {

    private final static String KEY_USERNAME = "KEY_USERNAME";

    protected Button dialog1Button;
    protected Button dialog2Button;
    protected Button dialog3Button;
    protected Button saveToFileButton;

    protected boolean isAlive = false;
    DownloadAsyncTask downloadAsyncTask;

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        isAlive = true;

        dialog1Button = findViewById(R.id.dialog1Button);
        dialog2Button = findViewById(R.id.dialog2Button);
        dialog3Button = findViewById(R.id.dialog3Button);
        saveToFileButton = findViewById(R.id.saveToFileButton);

        dialog1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PromptDialogFragment
                        .newInstance()
                        .show(getSupportFragmentManager(), "dialog");

            }
        });

        dialog2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadAsyncTask = new DownloadAsyncTask();
                downloadAsyncTask.execute(Uri.parse("http://google.ru"));
            }
        });

        dialog3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("settings", 0);

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("User Name")
                        .setMessage(sharedPreferences.getString(KEY_USERNAME, ""))
                        .setPositiveButton("Ok", null)
                        .show();
            }
        });

        saveToFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    File profileFile = new File(MainActivity.this.getFilesDir(), "profile.json");
                    FileOutputStream outputStream = new FileOutputStream(profileFile);
                    PrintStream printStream = new PrintStream(outputStream);
                    printStream.append("{\"id\": \"123\"}");
                    printStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final EditText editText = new EditText(MainActivity.this);
                editText.setLayoutParams(new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("What is your name?")
//                        .setMessage("Are you sure?")
                        .setCancelable(false)
                        .setView(editText)
                        .setPositiveButton("Set Name", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences sharedPreferences = getSharedPreferences("settings", 0);
                                sharedPreferences
                                        .edit()
                                        .putString(KEY_USERNAME, editText.getText().toString())
                                        .commit();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
                            }
                        })
                        .show();

            }
        });


        SharedPreferences sharedPreferences = getSharedPreferences("settings", 0);
        sharedPreferences
                .edit()
                .putString(KEY_USERNAME, "No Name")
                .commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isAlive = false;
        if (downloadAsyncTask != null) {
            downloadAsyncTask.cancel(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCancel() {
        Toast.makeText(this, "cancel", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDelete() {
        Toast.makeText(this, "delete", Toast.LENGTH_LONG).show();
    }

    class DownloadAsyncTask extends AsyncTask<Uri, Integer, File> {

        protected android.app.ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected File doInBackground(Uri... uris) {
            try {
                for (int i = 0; i < 100; i++) {
                    publishProgress(i);
                    Thread.sleep(300);
                }
            } catch (InterruptedException e) {
                return null;
            }
            return new File("/downloaded_file.mp4");
        }

        @Override
        protected void onPostExecute(File file) {
            progressDialog.dismiss();
            if (file != null && MainActivity.this.isAlive) {
                Toast.makeText(MainActivity.this, "filedonwloaded", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressDialog.setProgress(values[0]);
        }
    }
}
